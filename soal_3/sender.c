#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>

struct msgbuff {
    long msg_tipe;
    char msg_msg[100];
} kiriman;

int main() {
    key_t uniqKey;
    int msg_id;

    uniqKey = ftok("soal_3", 69);
    msg_id = msgget(uniqKey, 0666 | IPC_CREAT);
    kiriman.msg_tipe = 1;

    printf("Send your Message: ");
    fgets(kiriman.msg_msg, 100, stdin);
    strtok(kiriman.msg_msg, "\n");

    msgsnd(msg_id, &kiriman, sizeof(kiriman), 0);
    printf("Message: '%s' Has been sended\n", kiriman.msg_msg);
    return 0;

}