#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

char userfile[] = "users/users.txt";

static const char b64_char[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZzbcdefghijklmnopqrstuvwxyz0123456789+/";

static int b64_cek(char k) {
    if(k >= 'A' && k <= 'Z') return k - 'A';
    else if (k >= 'a' && k <= 'z') return k - 'a' + 26;
    else if (k >= '0' && k <= '9') return k - '0' + 52;
    else if (k == '+') return 62;
    else if (k == '/') return 63;
    else return -1;
}

char *b64_decode(const char *kata) {
    char b64_part[4];
    int len_kata = strlen(kata);
    int len_hasil = (len_kata / 4)*3;

    if(len_kata % 4 != 0) return NULL;
    if(kata[len_kata-1] == '=') len_hasil--;
    if(kata[len_kata-2] == '=') len_hasil--;

    char *hasil = malloc(len_hasil+1);
    int i, j;

    for(i=0, j=0; i<len_kata; i=i+4, j=j+3) {
        b64_part[0] = b64_cek(kata[i]);
        b64_part[1] = b64_cek(kata[i+1]);
        b64_part[2] = b64_cek(kata[i+2]);
        b64_part[3] = b64_cek(kata[i+3]);

        if(kata[i+1] != '=') hasil[j] = (b64_part[0] << 2) | (b64_part[1] >> 4);
        if(kata[i+2] != '=') hasil[j+1] = (b64_part[1] << 4) | (b64_part[2] >> 2);
        if(kata[i+3] != '=') hasil[j+2] = (b64_part[2] << 6) | b64_part[3];

    }
    
    hasil[len_hasil] = '\0';
    return hasil;

}

struct daftarUser {
    char nama[20];
    char pass[20];
};

struct daftarUser pengguna[20];
struct daftarUser userNow;
int jmlahUser = 0;

void userlist() {
    jmlahUser = 0;
    char buffah[100];
    FILE *opened = fopen(userfile, "r");
    
    while(fgets(buffah, sizeof(buffah), opened) != NULL) {
        char *cheng;
        if((cheng = strchr(buffah, '\n')) != NULL) *cheng = '\0';

        char *nama = strtok(buffah, ":");
        char *pass = strtok(NULL, ":");

        char *newpass = b64_decode(pass);

        strcpy(pengguna[jmlahUser].nama, nama);
        strcpy(pengguna[jmlahUser].pass, newpass);
        free(newpass);
        jmlahUser++;
    }

    fclose(opened);
}

void creds() {
    userlist();
    for(int i = 0; i<jmlahUser; i++) {
        printf("Username: %s, Password: %s\n", pengguna[i].nama, pengguna[i].pass);
    }
    return;
}

int loging(const char *uname, const char *passw) {
    int i = 0;
    for(i = 0; i<jmlahUser; i++) {
        int adaNama = strcmp(uname, pengguna[i].nama);

        if (adaNama == 0) {
            if(strcmp(passw, pengguna[i].pass) == 0) {
                strcpy(userNow.nama, uname);
                strcpy(userNow.pass, passw);
                
                if(userNow.nama[0] != '\0') printf("Authentication Successful\n");
                return 1;
            } else {
                printf("Authentication Failed\n");
                return 0;
            }
        }
    }
    if(i == jmlahUser && userNow.nama[0] == '\0') printf("Authentication Failed\n");
    return 0;
}

int fileAvailable(const char *fileToSen) {
    char senDir[50] = "Sender/";
    char recivDir[40] = "Receiver/";
    strcat(senDir, fileToSen);
    strcat(recivDir, fileToSen);
    FILE* fileptr;

    fileptr = fopen(senDir, "r");
    if(fileptr == NULL) {
        printf("%s is Not Exist\n", fileToSen);
        fclose(fileptr);
        return 0;
    }
    
    fileptr = fopen(recivDir, "r");
    if(fileptr != NULL) {
        printf("%s is Already Sended\n", fileToSen);
        fclose(fileptr);
        return 0;
    }

    return 1;
}

off_t cekSize(const char *namaFile) {
    struct stat fileStat;
    if(stat(namaFile, &fileStat) == 0) {
        return fileStat.st_size;
    }
    return 0;
}

void sendFile(const char *oneFile) {
    if(userNow.nama[0] == '\0') {
        printf("Please login first\n");
        return;
    }

    char senDir[50] = "Sender/";
    char recivDir[50] = "Receiver/";
    strcat(senDir, oneFile);
    strcat(recivDir, oneFile);
    
    if(fileAvailable(oneFile)) {
        off_t filSiz = cekSize(senDir);

        pid_t cabang = fork();
        if(cabang == 0) {
            char *argv[] = {"cp", senDir, recivDir, NULL};
            execv("/bin/cp", argv);
        } else {
            printf("%s (%jd KB) Has Been Sended\n", oneFile, filSiz);
        }
    }

}

struct received {
    long msg_tipe;
    char msg_msg[100];
} kiriman;


int main() {
    key_t uniqKey;
    int msg_id;

    uniqKey = ftok("soal_3", 69);
    msg_id = msgget(uniqKey, 0666 | IPC_CREAT);

    while(1) {
        char koman[100];
        msgrcv(msg_id, &kiriman, sizeof(kiriman), 1, 0);
        strcpy(koman, kiriman.msg_msg);

        strtok(koman, "\n");
        char *arg1 = strtok(koman, " ");
        
        if(strcmp(arg1, "CREDS") == 0) {
            creds();

        } else if(strcmp(arg1, "AUTH:") == 0) {
            arg1 = strtok(NULL, " ");
            char *arg2 = arg1;
            arg1 = strtok(NULL, " ");
            char *arg3 = arg1;

            if(arg2 == NULL || arg3 == NULL) {
                printf("UNKNOWN COMMAND\n");
                return 0;
            }
            int sttus = loging(arg2, arg3);
            if(sttus == 0) break;
            
        } else if(strcmp(arg1, "TRANSFER") == 0) {
            arg1 = strtok(NULL, " ");
            char *arg2 = arg1;

            sendFile(arg2);
        } else if(strcmp(arg1, "QUIT") == 0) {
            break;
        } else {
            printf("UNKNOWN COMMAND\n");
        }
    }

    printf("Turned off the Receiver...\n");
    msgctl(msg_id, IPC_RMID, NULL);
    return 0;
}