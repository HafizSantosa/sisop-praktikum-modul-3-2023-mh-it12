# sisop-praktikum-modul-3-2023-MH-IT12

Laporan Resmi Praktikum Sistem Operasi Modul 3
Kelompok IT12:

+ Muhammad Afif (5027221032)
+ Hafiz Akmaldi Santosa (5027221061)
+ Nur Azka Rahadiansyah (5027221064)


Pengantar

Laporan resmi ini dibuat terkait dengan praktikum modul 3 sistem operasi yang telah dilaksanakan pada tanggal xx hingga tanggal xx. Praktikum modul 3 terdiri dari 4 soal dengan tema C yang dikerjakan oleh kelompok praktikan yang terdiri dari 3 orang selama waktu tertentu.
Kelompok IT12 melakukan pengerjaan modul 3 ini dengan pembagian sebagai berikut:


- Soal 1 dikerjakan oleh Nur Azka, Hafiz A., dan M.Afif 
- Soal 2 dikerjakan oleh Nur Azka
- Soal 3 dikerjakan oleh M.Afif
- Soal 4 dikerjakan oleh Hafiz A.

Sehingga dengan demikian, semua anggota kelompok memiliki peran yang sama besar dalam pengerjaan modul 2 ini.

Kelompok IT12 juga telah menyelesaikan tugas praktikum modul 3 yang telah diberikan dan telah melakukan demonstrasi kepada Asisten lab penguji Mas Rifqi. Dari hasil praktikum yang telah dilakukan sebelumnya, maka diperoleh hasil sebagaimana yang dituliskan pada setiap bab di bawah ini.

## Soal 1

Soal ini bermaksud untuk membuat 3 file c dengan ketentuan sebagai berikut:
1. belajar.c - untuk membuat matriks berukuran [digit_satuan_kelompok] X 2 dan 2 X [digit_satuan_kelompok] lalu mengalikannya dan kemudian men-transposenya. Hasil matriks kemudian di letakkan pada sebuah shared memory agar dapat diakses oleh file c lain.
2. yang.c - mengubah elemen pada matriks dalam shared memori menjadi hasil faktorial masing-masing elemennya. Perhitungan faktorial menggunakan konsep multithreading agar dapat selesai dengan cepat.
3. rajin.c - sama seperti yang.c namun tidak menggunakan konsep multithreading. Tujuannya adalah untuk melihat program mana yang dapat menyelesaikan proses terlebih dulu.


### Isi soal
Epul memiliki seorang adik SMA bernama Azka. Azka bersekolah di Thursina Malang. Di sekolah, Azka diperkenalkan oleh adanya kata matriks. Karena Azka baru pertama kali mendengar kata matriks, Azka meminta tolong kepada kakaknya Epul untuk belajar matriks.  Karena Epul adalah seorang kakak yang baik, Epul memiliki ide untuk membuat program yang bisa membantu adik tercintanya, tetapi dia masih bingung untuk membuatnya. Karena Epul adalah bagian dari IT05 dan praktikan sisop, sebagai warga IT05 yang baik, bantu Epul mengerjakan programnya dengan ketentuan sebagai berikut :

+ Membuat program C dengan nama `belajar.c`, yang berisi program untuk melakukan perkalian matriks. Agar ukuran matriks bervariasi, maka ukuran matriks pertama adalah [`nomor_kelompok`]×2 dan matriks kedua 2×[`nomor_kelompok`]. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-4 (inklusif), dan rentang pada matriks kedua adalah 1-5 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar. 

+ Karena Epul merasa ada yang kurang, Epul meminta agar hasil dari perkalian tersebut dikurang 1 di setiap matriks.

+ Karena Epul baru belajar modul 3, Epul ngide ingin meminta agar menerapkan konsep shared memory. Buatlah program C kedua dengan nama `yang.c`. Program ini akan mengambil variabel hasil pengurangan dari perkalian matriks dari program `belajar.c` (program sebelumnya). Hasil dari pengurangan perkalian matriks tersebut dilakukan transpose matriks dan diperlihatkan hasilnya. (Catatan: wajib menerapkan konsep shared memory)

+ Setelah ditampilkan, Epul pengen adiknya belajar lebih, sehingga untuk setiap angka dari transpose matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks. Contoh: 
```c
matriks 
1	2  	3
2	2	4

maka:
1	4	6
4	4	24
```
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)
+ Epul penasaran mengapa dibuat thread dan multithreading pada program sebelumnya, dengan demikian dibuatlah program C ketiga dengan nama `rajin.c`. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada `yang.c` namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. Dokumentasikan dan sampaikan saat demo dan laporan resmi.

### Catatan:
- Key memory diambil berdasarkan program dari `belajar.c` sehingga program `belajar.c` tidak perlu menghasilkan key memory (value). Dengan demikian, pada program `yang.c` dan `rajin.c` hanya perlu mengambil key memory dari `belajar.c`
- Untuk kelompok 1 - 9, 11 - 19, 21 menggunakan  digit akhir saja.
	Contoh : 19 -> 9
- Untuk kelompok 10 dan 20 menambahkan digit awal dan akhir.
	Contoh : 20 - > 2



### Penyelesaian
`belajar.c`
```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#define ROWS1 2
#define COLS1 2
#define ROWS2 2
#define COLS2 2

int main() {
    int matrix1[ROWS1][COLS1];
    int matrix2[ROWS2][COLS2];

    srand(time(NULL));
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS1; j++) {
            matrix1[i][j] = rand() % 4 + 1;
        }
    }

    for (int i = 0; i < ROWS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            matrix2[i][j] = rand() % 5 + 1;
        }
    }

    key_t key;

    key = ftok("belajar.c", 'R');
    int shmid = shmget(key, sizeof(int) * ROWS1 * COLS2, 0666 | IPC_CREAT);
    int *result = (int *)shmat(shmid, (void *)0, 0);

    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            result[i * COLS2 + j] = 0;
            for (int k = 0; k < COLS1; k++) {
                result[i * COLS2 + j] += matrix1[i][k] * matrix2[k][j];
            }
            result[i * COLS2 + j] -= 1;
        }
    }

    printf("Matriks Hasil:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", result[i * COLS2 + j]);
        }
        printf("\n");
    }

    int transpose[COLS2][ROWS1];
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            transpose[j][i] = result[i * COLS2 + j];
        }
    }

    printf("Matriks Hasil Transpose:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < ROWS1; j++) {
            printf("%d ", transpose[i][j]);
        }
        printf("\n");
    }

    for(int i=0; i<COLS2;i++) {
        for(int j=0; j<ROWS2;j++) {
            result[i * COLS2 + j] = transpose[i][j];
        }
    }

    shmdt(result);

    return 0;
}
```
`yang.c`
```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#define ROWS1 2
#define COLS2 2

unsigned long long factorial(int n) {
    unsigned long long hasil = 1;
    for (int i = 1; i <= n; i++) {
        hasil *= i;
    }
    return hasil;
}

void *calculate_factorial(void *arg) {
    int *data = (int *)arg;
    data[0] = factorial(data[0]);
    pthread_exit(NULL);
}

int main() {
    clock_t tic = clock();
    key_t key = ftok("belajar.c", 'R');

    int shmid = shmget(key, sizeof(int) * ROWS1 * COLS2, 0666);
    int *result = (int *)shmat(shmid, (void *)0, 0);
    
    pthread_t threads[ROWS1 * COLS2];
    int thread_args[ROWS1 * COLS2];

    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            thread_args[i * COLS2 + j] = result[i * COLS2 + j];
            pthread_create(&threads[i * COLS2 + j], NULL, calculate_factorial, &thread_args[i * COLS2 + j]);
        }
    }
    for (int i = 0; i < ROWS1 * COLS2; i++) {
        pthread_join(threads[i], NULL);
    }

    printf("Matriks Hasil Faktorial:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%llu ", (unsigned long long)thread_args[i * COLS2 + j]);
        }
        printf("\n");
    }

    clock_t toc = clock();
    printf("\nprogram berjalan selama %f detik.\n", (double)(toc-tic) / CLOCKS_PER_SEC );
    
    shmdt(result);

    return 0;
}

```
`rajin.c`
```c
#include <stdio.h>
#include <sys/shm.h>
#include <time.h>

#define ROWS1 2
#define COLS2 2

unsigned long long factorial(int n) {
    unsigned long long hasil = 1;
    for (int i = 1; i <= n; i++) {
        hasil *= i;
    }
    return hasil;
}

int main() {

    clock_t tic = clock();
    key_t key = ftok("belajar.c", 'R');

    int shmid = shmget(key, sizeof(int) * ROWS1 * COLS2, 0666);
    int *result = (int *)shmat(shmid, (void *)0, 0);
    int thread_args[ROWS1 * COLS2];

    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            thread_args[i * COLS2 + j] = factorial(result[i * COLS2 + j]);
        }
    }

    printf("Matriks Hasil Faktorial:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%llu ", (unsigned long long)thread_args[i * COLS2 + j]);
        }
        printf("\n");
    }

    clock_t toc = clock();
    printf("\nprogram berjalan selama %f detik.\n", (double)(toc-tic) / CLOCKS_PER_SEC );

    shmdt(result);

    return 0;
}
```
### Penjelasan
`belajar.c`
```c
part code here
```
lorem ipsum dst dst dst

`yang.c`
```c
part code here
```
lorem ipsum dst dst dst

`rajin.c`
```c
part code here
```
lorem ipsum dst dst dst

## Soal 2
 membuat program "8ballondors.c" untuk menghitung jumlah kemunculan kata atau huruf dalam lirik lagu favoritnya. Program ini menggunakan konsep pipes dan fork, dengan parent process yang membersihkan karakter-karakter non-huruf dari lirik lagu dan membuat berkas "thebeatles.txt," sementara child process menghitung frekuensi kemunculan kata atau huruf sesuai argumen yang diberikan oleh pengguna. Hasil perhitungan kemudian dicatat dalam berkas log dengan format tanggal, jenis (KATA atau HURUF), dan pesan yang mencantumkan kata atau huruf yang dihitung serta jumlahnya. Program ini melakukan pencarian case-sensitive, memperlakukan huruf besar dan kecil sebagai berbeda dalam perhitungan frekuensi.
### Isi soal
Messi the only GOAT sedang gabut karena Inter Miami Gagal masuk Playoff. Messi menghabiskan waktu dengan mendengar lagu. Karena gabut yang tergabut gabut, Messi penasaran berapa banyak jumlah kata tertentu yang muncul pada lagu tersebut. Messi mencoba ngoding untuk mencari jawaban dari kegabutannya. Pertama-tama dia mengumpulkan beberapa lirik lagu favorit yang disimpan di file. Beberapa saat setelah mencoba ternyata ngoding tidak semudah itu, Messipun meminta tolong kepada Ronaldo, tetapi karena Ronaldo sibuk main di liga oli, Ronaldopun meminta tolong kepadamu untuk dibuatkan kode dari program Messi dengan ketentuan sebagai berikut : 
(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).

a. Messi ingin nama programnya 8ballondors.c. Pada parent process, program akan membaca file dan menghapus karakter-karakter yang bukan huruf pada file dan membuat output file dari hasil proses itu dengan nama thebeatles.txt

b. Pada child process, lakukan perhitungan jumlah frekuensi kemunculan sebuah kata dari output file yang akan diinputkan (kata) oleh user.

c. Karena gabutnya, Messi ingin program dapat mencari jumlah frekuensi kemunculan sebuah huruf dari file. Maka, pada child process lakukan perhitungan jumlah frekuensi kemunculan sebuah huruf dari output file yang akan diinputkan (huruf) user.

d. Karena terdapat dua perhitungan, maka pada program buatlah argumen untuk menjalankan program : 
i.  Kata	: ./8ballondors -kata
ii. Huruf	: ./8ballondors -huruf

e. Hasil dari perhitungan  jumlah frekuensi kemunculan sebuah kata dan  jumlah frekuensi kemunculan sebuah huruf dikirim ke parent process.

f. Messi ingin agar setiap kata atau huruf dicatat dalam sebuah log yang diberi nama frekuensi.log. Pada parent process, lakukan pembuatan file log berdasarkan data yang dikirim dari child process. 

i.  Format: [date] [type] [message]

ii. Type: KATA, HURUF

iii. Ex:
1. [24/10/23 01:05:48] [KATA] Kata 'yang' muncul sebanyak 10 kali dalam file 'thebeatles.txt'
2. [24/10/23 01:04:29] [HURUF] Huruf 'a' muncul sebanyak 396 kali dalam file 'thebeatles.txt'

### Catatan:
Perhitungan jumlah frekuensi kemunculan kata atau huruf  menggunakan Case-Sensitive Search. 

### penyelesaian

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>

#define LOG_FILE "frekuensi.log"

void addLog(const char* tipe, const char* teks) {
    time_t terkini;
    char timestamp[20];

    time(&terkini);
    strftime(timestamp, 20, "[%d/%m/%y %H:%M:%S]", localtime(&terkini));

    FILE* log_file = fopen(LOG_FILE, "a");
    if (log_file) fprintf(log_file, "%s [%s] %s\n", timestamp, tipe, teks);
    fclose(log_file);
}

void getLirik() {
    FILE* cekFile = fopen("lyrics.txt", "a");
    char cmdDonlod[256];
    
}

void getOutput() {
    getLirik();

    FILE *input_file = fopen("lyrics.txt", "r");
    FILE *output_file = fopen("thebeatles.txt", "w");
    int k;

    if (input_file == NULL || output_file == NULL) {
        perror("Failed to open file");
        exit(1);
    }

    while ((k = fgetc(input_file)) != EOF) {
        if (isalpha(k) || k == ' ' || k == '\n') {
            if (isupper(k)) k = tolower(k);
            fputc(k, output_file);
        }
    }
    fclose(input_file);
    fclose(output_file);
}

void hitungHruf(const char* hruuf) {
    char fileCek[100];
    char riilHruuf = hruuf[0];
    int jmlah = 0;
    sprintf(fileCek, "thebeatles.txt");
    FILE *toCek = fopen(fileCek, "r");

    if (toCek == NULL) {
        perror("open file error\n");
        exit(1);
    }

    int c;
    while ((c = fgetc(toCek)) != EOF) {
        if (c == riilHruuf) jmlah++;
    }
    fclose(toCek);

    char mesg[512];
    snprintf(mesg, sizeof(mesg), "Huruf '%c' muncul sebanyak %d kali dalam file '%s'", riilHruuf, jmlah, fileCek);

    addLog("HURUF", mesg);
    printf("%s\n", mesg);
}

void hitungKta(const char* word) {
    char fileCek[100];
    char cariKata[256];
    char kataNow[256] = {0};
    int jmlah = 0;
    int inKata = 0;
    int pnjangKata = 0;

    sprintf(fileCek, "thebeatles.txt");
    sprintf(cariKata, " %s ", word);

    FILE *toCek = fopen(fileCek, "r");
    if (toCek == NULL) {
        perror("open file error\n");
        exit(1);
    }

    int c;
    while ((c = fgetc(toCek)) != EOF) {
        if (isalpha(c)) {
            kataNow[pnjangKata++] = c;
            inKata = 1;
        } else {
            if (inKata) {
                kataNow[pnjangKata] = '\0';
                if (strcmp(kataNow, word) == 0) jmlah++;

                pnjangKata = 0;
                inKata = 0;
            }
        }
    }
    fclose(toCek);

    char mesg[512];
    snprintf(mesg, sizeof(mesg), "Kata '%s' muncul sebanyak %d kali dalam file '%s'", word, jmlah, fileCek);
    addLog("KATA", mesg);
    printf("%s\n", mesg);
}

int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) {
        printf("Format: %s [<-kata> atau <-huruf>]\n", argv[0]);
        return 1;
    }

    getOutput();

    int pipefd[2];
    pipe(pipefd);
    pid_t child_pid = fork();

    if (child_pid == 0) {
        close(pipefd[0]);

        if (strcmp(argv[1], "-huruf") == 0) {
            char letter[256];
            printf("Masukkan huruf: ");
            scanf("%s", letter);
            write(pipefd[1], letter, strlen(letter) + 1);

        } else if (strcmp(argv[1], "-kata") == 0) {
            char word[256];
            printf("Masukkan kata: ");
            scanf("%s", word);
            write(pipefd[1], word, strlen(word) + 1);

        }
        close(pipefd[1]);
        exit(0);

    } else {
        close(pipefd[1]);
        int status;
        wait(&status);

        char dicari[256];
        read(pipefd[0], dicari, 256);

        if (strcmp(argv[1], "-huruf") == 0) hitungHruf(dicari);
        else if (strcmp(argv[1], "-kata") == 0) hitungKta(dicari);
    }

    return 0;
}
```

# penjelasan

### addLog(const char* pipe, const char* teks):
```
void addLog(const char* tipe, const char* teks) {
    time_t terkini;
    char timestamp[20];

    time(&terkini);
    strftime(timestamp, 20, "[%d/%m/%y %H:%M:%S]", localtime(&terkini));

    FILE* log_file = fopen(LOG_FILE, "a");
    if (log_file) fprintf(log_file, "%s [%s] %s\n", timestamp, tipe, teks);
    fclose(log_file);
}
```

Fungsi ini digunakan untuk menambahkan entri log ke dalam berkas "frekuensi.log".
Parameter tipe digunakan untuk menentukan jenis log (misalnya "HURUF" atau "KATA").
Parameter teks berisi pesan log yang akan ditambahkan.
Fungsi ini mendapatkan waktu saat ini, memformatnya dalam bentuk timestamp, dan kemudian mencetak pesan log beserta timestamp ke dalam berkas log.

### getLirik():
```
void getLirik() {
    FILE* cekFile = fopen("lyrics.txt", "a");
    char cmdDonlod[256];
    
}
```


Fungsi ini seharusnya digunakan untuk mengunduh teks lirik lagu (namun belum diimplementasikan dengan baik dalam kode).
Sebaiknya fungsi ini diperbaiki untuk mengunduh teks lirik dari sumber yang sesuai dan menyimpannya dalam berkas "lyrics.txt".

### getOutput():
```
void getOutput() {
    getLirik();

    FILE *input_file = fopen("lyrics.txt", "r");
    FILE *output_file = fopen("thebeatles.txt", "w");
    int k;

    if (input_file == NULL || output_file == NULL) {
        perror("Failed to open file");
        exit(1);
    }

    while ((k = fgetc(input_file)) != EOF) {
        if (isalpha(k) || k == ' ' || k == '\n') {
            if (isupper(k)) k = tolower(k);
            fputc(k, output_file);
        }
    }
    fclose(input_file);
    fclose(output_file);
}
```



Fungsi ini digunakan untuk menghasilkan berkas "thebeatles.txt" yang telah diubah, yaitu teks yang hanya berisi huruf kecil dan spasi.
Fungsi membaca teks dari berkas "lyrics.txt" dan mengonversi huruf besar menjadi huruf kecil.
Hasil konversi disimpan dalam berkas "thebeatles.txt".

### hitungHruf(const char* hruuf):
```
void hitungHruf(const char* hruuf) {
    char fileCek[100];
    char riilHruuf = hruuf[0];
    int jmlah = 0;
    sprintf(fileCek, "thebeatles.txt");
    FILE *toCek = fopen(fileCek, "r");

    if (toCek == NULL) {
        perror("open file error\n");
        exit(1);
    }

    int c;
    while ((c = fgetc(toCek)) != EOF) {
        if (c == riilHruuf) jmlah++;
    }
    fclose(toCek);

    char mesg[512];
    snprintf(mesg, sizeof(mesg), "Huruf '%c' muncul sebanyak %d kali dalam file '%s'", riilHruuf, jmlah, fileCek);

    addLog("HURUF", mesg);
    printf("%s\n", mesg);
}
```


Fungsi ini menghitung jumlah kemunculan sebuah huruf tertentu dalam teks yang ada di "thebeatles.txt".
Parameter hruuf adalah huruf yang akan dihitung kemunculannya.
Fungsi membuka berkas "thebeatles.txt", membaca karakter-karakter dalam berkas, dan menghitung jumlah kemunculan huruf tersebut.
Hasil perhitungan dicatat dalam pesan log dan juga ditampilkan di layar.


### hitungKta(const char* word):
```
void hitungKta(const char* word) {
    char fileCek[100];
    char cariKata[256];
    char kataNow[256] = {0};
    int jmlah = 0;
    int inKata = 0;
    int pnjangKata = 0;

    sprintf(fileCek, "thebeatles.txt");
    sprintf(cariKata, " %s ", word);

    FILE *toCek = fopen(fileCek, "r");
    if (toCek == NULL) {
        perror("open file error\n");
        exit(1);
    }

    int c;
    while ((c = fgetc(toCek)) != EOF) {
        if (isalpha(c)) {
            kataNow[pnjangKata++] = c;
            inKata = 1;
        } else {
            if (inKata) {
                kataNow[pnjangKata] = '\0';
                if (strcmp(kataNow, word) == 0) jmlah++;

                pnjangKata = 0;
                inKata = 0;
            }
        }
    }
    fclose(toCek);

    char mesg[512];
    snprintf(mesg, sizeof(mesg), "Kata '%s' muncul sebanyak %d kali dalam file '%s'", word, jmlah, fileCek);
    addLog("KATA", mesg);
    printf("%s\n", mesg);
}
```


Fungsi ini menghitung jumlah kemunculan sebuah kata tertentu dalam teks yang ada di "thebeatles.txt".
Parameter word adalah kata yang akan dihitung kemunculannya.
Fungsi membuka berkas "thebeatles.txt", membaca karakter-karakter dalam berkas, dan menghitung jumlah kemunculan kata tersebut.
Hasil perhitungan dicatat dalam pesan log dan juga ditampilkan di layar.


### main(int argc, char *argv[]):
```
int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) {
        printf("Format: %s [<-kata> atau <-huruf>]\n", argv[0]);
        return 1;
    }

    getOutput();

    int pipefd[2];
    pipe(pipefd);
    pid_t child_pid = fork();

    if (child_pid == 0) {
        close(pipefd[0]);

        if (strcmp(argv[1], "-huruf") == 0) {
            char letter[256];
            printf("Masukkan huruf: ");
            scanf("%s", letter);
            write(pipefd[1], letter, strlen(letter) + 1);

        } else if (strcmp(argv[1], "-kata") == 0) {
            char word[256];
            printf("Masukkan kata: ");
            scanf("%s", word);
            write(pipefd[1], word, strlen(word) + 1);

        }
        close(pipefd[1]);
        exit(0);

    } else {
        close(pipefd[1]);
        int status;
        wait(&status);

        char dicari[256];
        read(pipefd[0], dicari, 256);

        if (strcmp(argv[1], "-huruf") == 0) hitungHruf(dicari);
        else if (strcmp(argv[1], "-kata") == 0) hitungKta(dicari);
    }

    return 0;
}

```


fungsi ini memeriksa jumlah argumen yang diberikan pada baris perintah. Program membutuhkan satu argumen yang dapat berupa -kata atau -huruf.
Fungsi ini menggunakan fork untuk membuat proses anak yang akan menerima input dari pengguna.
Bergantung pada argumen yang diberikan, pengguna diminta memasukkan huruf atau kata.
Setelah pengguna memasukkan input, proses anak akan menutup saluran keluar dan mengirim input ke proses utama melalui saluran masuk.
Proses utama akan menunggu hingga proses anak selesai, kemudian melakukan perhitungan jumlah huruf atau kata sesuai dengan argumen yang diberikan.
Hasil perhitungan ditampilkan di layar dan juga dicatat dalam berkas log.


## Soal 3
(Afif)
Soal ini bermaksud untuk membuat dua file c yang berfungsi sebagai sender message dan receiver message. Sender digunakan untuk mengirim perintah dan Receiver akan menjalankannya.

### Isi soal
Christopher adalah seorang praktikan sisop, dia mendapat tugas dari pak Nolan untuk membuat komunikasi antar proses dengan menerapkan konsep message queue. Pak Nolan memberikan kredensial list  users yang harus masuk ke dalam program yang akan dibuat. Lebih lanjutnya pak Nolan memberikan instruksi tambahan sebagai berikut :

+ Bantulah Christopher untuk membuat program tersebut, dengan menerapkan konsep message queue(wajib) maka buatlah 2  program, `sender.c` sebagai pengirim dan `receiver.c` sebagai penerima. Dalam hal ini, sender hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh receiver.
Struktur foldernya akan menjadi seperti berikut
```c
└── soal3
	├── users
	├── receiver.c
	└── sender.c
```
+ Ternyata list kredensial yang diberikan Pak Nolan semua passwordnya terenkripsi dengan menggunakan base64. Untuk mengetahui kredensial yang valid maka Sender pertama kali akan mengirimkan perintah `CREDS` kemudian sistem receiver akan melakukan decrypt/decode/konversi pada file users, lalu menampilkannya pada receiver. Hasilnya akan menjadi seperti berikut :
```c
./receiver
Username: Mayuri, Password: TuTuRuuu
Username: Onodera, Password: K0sak!
Username: Johan, Password: L!3b3rt
Username: Seki, Password: Yuk!n3
Username: Ayanokouji, Password: K!yot4kA
...

```
+ Setelah mengetahui list kredensial, bantulah christopher untuk membuat proses autentikasi berdasarkan list kredensial yang telah disediakan. Proses autentikasi dilakukan dengan menggunakan perintah `AUTH: username password` kemudian jika proses autentikasi valid dan berhasil maka akan menampilkan Authentication successful . Authentication failed jika gagal.

+ Setelah berhasil membuat proses autentikasi, buatlah proses transfer file. Transfer file dilakukan dari direktori Sender dan dikirim ke direktori Receiver . Proses transfer dilakukan dengan menggunakan perintah `TRANSFER filename` . Struktur direktorinya akan menjadi seperti berikut:
```c
└── soal3
       ├── Receiver
       ├── Sender
       ├── receiver.c
       └── sender.c
```
+ Karena takut memorinya penuh, Christopher memberi status size(kb) pada setiap pengiriman file yang berhasil.

+ Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan `UNKNOWN COMMAND`.

### Catatan:
- Dilarang untuk overwrite file users secara permanen
- Ketika proses autentikasi gagal program receiver akan mengirim status `Authentication failed` dan program langsung keluar
- Sebelum melakukan transfer file sender harus login (melalui proses auth) terlebih dahulu
- Buat transfer file agar tidak duplicate dengan file yang memang sudah ada atau sudah pernah dikirim.


### Penyelesaian
`sender.c`
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>

struct msgbuff {
    long msg_tipe;
    char msg_msg[100];
} kiriman;

int main() {
    key_t uniqKey;
    int msg_id;

    uniqKey = ftok("soal_3", 69);
    msg_id = msgget(uniqKey, 0666 | IPC_CREAT);
    kiriman.msg_tipe = 1;

    printf("Send your Message: ");
    fgets(kiriman.msg_msg, 100, stdin);
    strtok(kiriman.msg_msg, "\n");

    msgsnd(msg_id, &kiriman, sizeof(kiriman), 0);
    printf("Message: '%s' Has been sended\n", kiriman.msg_msg);
    return 0;

}
```
`receiver.c`
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

char userfile[] = "users/users.txt";

static const char b64_char[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZzbcdefghijklmnopqrstuvwxyz0123456789+/";

static int b64_cek(char k) {
    if(k >= 'A' && k <= 'Z') return k - 'A';
    else if (k >= 'a' && k <= 'z') return k - 'a' + 26;
    else if (k >= '0' && k <= '9') return k - '0' + 52;
    else if (k == '+') return 62;
    else if (k == '/') return 63;
    else return -1;
}

char *b64_decode(const char *kata) {
    char b64_part[4];
    int len_kata = strlen(kata);
    int len_hasil = (len_kata / 4)*3;

    if(len_kata % 4 != 0) return NULL;
    if(kata[len_kata-1] == '=') len_hasil--;
    if(kata[len_kata-2] == '=') len_hasil--;

    char *hasil = malloc(len_hasil+1);
    int i, j;

    for(i=0, j=0; i<len_kata; i=i+4, j=j+3) {
        b64_part[0] = b64_cek(kata[i]);
        b64_part[1] = b64_cek(kata[i+1]);
        b64_part[2] = b64_cek(kata[i+2]);
        b64_part[3] = b64_cek(kata[i+3]);

        if(kata[i+1] != '=') hasil[j] = (b64_part[0] << 2) | (b64_part[1] >> 4);
        if(kata[i+2] != '=') hasil[j+1] = (b64_part[1] << 4) | (b64_part[2] >> 2);
        if(kata[i+3] != '=') hasil[j+2] = (b64_part[2] << 6) | b64_part[3];

    }
    
    hasil[len_hasil] = '\0';
    return hasil;

}

struct daftarUser {
    char nama[20];
    char pass[20];
};

struct daftarUser pengguna[20];
struct daftarUser userNow;
int jmlahUser = 0;

void userlist() {
    jmlahUser = 0;
    char buffah[100];
    FILE *opened = fopen(userfile, "r");
    
    while(fgets(buffah, sizeof(buffah), opened) != NULL) {
        char *cheng;
        if((cheng = strchr(buffah, '\n')) != NULL) *cheng = '\0';

        char *nama = strtok(buffah, ":");
        char *pass = strtok(NULL, ":");

        char *newpass = b64_decode(pass);

        strcpy(pengguna[jmlahUser].nama, nama);
        strcpy(pengguna[jmlahUser].pass, newpass);
        free(newpass);
        jmlahUser++;
    }

    fclose(opened);
}

void creds() {
    userlist();
    for(int i = 0; i<jmlahUser; i++) {
        printf("Username: %s, Password: %s\n", pengguna[i].nama, pengguna[i].pass);
    }
    return;
}

int loging(const char *uname, const char *passw) {
    int i = 0;
    for(i = 0; i<jmlahUser; i++) {
        int adaNama = strcmp(uname, pengguna[i].nama);

        if (adaNama == 0) {
            if(strcmp(passw, pengguna[i].pass) == 0) {
                strcpy(userNow.nama, uname);
                strcpy(userNow.pass, passw);
                
                if(userNow.nama[0] != '\0') printf("Authentication Successful\n");
                return 1;
            } else {
                printf("Authentication Failed\n");
                return 0;
            }
        }
    }
    if(i == jmlahUser && userNow.nama[0] == '\0') printf("Authentication Failed\n");
    return 0;
}

int fileAvailable(const char *fileToSen) {
    char senDir[50] = "Sender/";
    char recivDir[40] = "Receiver/";
    strcat(senDir, fileToSen);
    strcat(recivDir, fileToSen);
    FILE* fileptr;

    fileptr = fopen(senDir, "r");
    if(fileptr == NULL) {
        printf("%s is Not Exist\n", fileToSen);
        fclose(fileptr);
        return 0;
    }
    
    fileptr = fopen(recivDir, "r");
    if(fileptr != NULL) {
        printf("%s is Already Sended\n", fileToSen);
        fclose(fileptr);
        return 0;
    }

    return 1;
}

off_t cekSize(const char *namaFile) {
    struct stat fileStat;
    if(stat(namaFile, &fileStat) == 0) {
        return fileStat.st_size;
    }
    return 0;
}

void sendFile(const char *oneFile) {
    if(userNow.nama[0] == '\0') {
        printf("Please login first\n");
        return;
    }

    char senDir[50] = "Sender/";
    char recivDir[50] = "Receiver/";
    strcat(senDir, oneFile);
    strcat(recivDir, oneFile);
    
    if(fileAvailable(oneFile)) {
        off_t filSiz = cekSize(senDir);

        pid_t cabang = fork();
        if(cabang == 0) {
            char *argv[] = {"cp", senDir, recivDir, NULL};
            execv("/bin/cp", argv);
        } else {
            printf("%s (%jd KB) Has Been Sended\n", oneFile, filSiz);
        }
    }

}

struct received {
    long msg_tipe;
    char msg_msg[100];
} kiriman;


int main() {
    key_t uniqKey;
    int msg_id;

    uniqKey = ftok("soal_3", 69);
    msg_id = msgget(uniqKey, 0666 | IPC_CREAT);

    while(1) {
        char koman[100];
        msgrcv(msg_id, &kiriman, sizeof(kiriman), 1, 0);
        strcpy(koman, kiriman.msg_msg);

        strtok(koman, "\n");
        char *arg1 = strtok(koman, " ");
        
        if(strcmp(arg1, "CREDS") == 0) {
            creds();

        } else if(strcmp(arg1, "AUTH:") == 0) {
            arg1 = strtok(NULL, " ");
            char *arg2 = arg1;
            arg1 = strtok(NULL, " ");
            char *arg3 = arg1;

            if(arg2 == NULL || arg3 == NULL) {
                printf("UNKNOWN COMMAND\n");
                return 0;
            }
            int sttus = loging(arg2, arg3);
            if(sttus == 0) break;
            
        } else if(strcmp(arg1, "TRANSFER") == 0) {
            arg1 = strtok(NULL, " ");
            char *arg2 = arg1;

            sendFile(arg2);
        } else if(strcmp(arg1, "QUIT") == 0) {
            break;
        } else {
            printf("UNKNOWN COMMAND\n");
        }
    }

    printf("Turned off the Receiver...\n");
    msgctl(msg_id, IPC_RMID, NULL);
    return 0;
}
```
### Penjelasan
`sender.c`
```c
struct msgbuff {
    long msg_tipe;
    char msg_msg[100];
} kiriman;
```
Bagian struct ini merupakan struktur pesan yang akan dikirim dalam bentuk message queue nantinya. struct ini diberi nama `kiriman`.

```c
int main() {
    key_t uniqKey;
    int msg_id;

    uniqKey = ftok("soal_3", 69);
    msg_id = msgget(uniqKey, 0666 | IPC_CREAT);
    kiriman.msg_tipe = 1;
```
Bagian ini adalah bagian pertama dari fungsi main. Bagian ini berfungsi untuk membentuk sebuah unique key yang kemudian digunakan sebagai id message yang akan dikirim.

```c
printf("Send your Message: ");
    fgets(kiriman.msg_msg, 100, stdin);
    strtok(kiriman.msg_msg, "\n");
```
Setelah dibentuk message id pada bagian sebelumnya, kini saatnya untuk menerima input pesan dari user. input dilakukan melalui fgets dan kemudian digunakan fungsi strtok untuk memotong karakter newline agar tidak masuk ke dalam pesan yang akan dikirim.

```c
    msgsnd(msg_id, &kiriman, sizeof(kiriman), 0);
    printf("Message: '%s' Has been sended\n", kiriman.msg_msg);
    return 0;
```
Pada bagian terakhir, pesan dikirim dalam sistem message queue dengan id sesuai yang telah dibuat sebelumnya. Sistem akan mengirimkan pesan `message [isi message] has been sended` dan kemudian sistem akan berhenti.

`receiver.c`

```c
char userfile[] = "users/users.txt";

static const char b64_char[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZzbcdefghijklmnopqrstuvwxyz0123456789+/";
```
Pada bagian paling atas, merupakan bagian deklarasi yang menyatakan isi variabel userfile dan menyatakan urutan karakter pada base64.

```c
static int b64_cek(char k) {
    if(k >= 'A' && k <= 'Z') return k - 'A';
    else if (k >= 'a' && k <= 'z') return k - 'a' + 26;
    else if (k >= '0' && k <= '9') return k - '0' + 52;
    else if (k == '+') return 62;
    else if (k == '/') return 63;
    else return -1;
}
```
Fungsi `b64_cek` digunakan untuk memeriksa nilai angka pada sebuah karakter dalam sebuah teks base64. apabila karakter yang diinputkan tidak terdaftar dalam daftar karakter base64, maka fungsi akan mengembalikan nilai -1 yang menandakan bahwa karakter tersebut tidak dapat ditemukan.

```c
char *b64_decode(const char *kata) {
    char b64_part[4];
    int len_kata = strlen(kata);
    int len_hasil = (len_kata / 4)*3;

    if(len_kata % 4 != 0) return NULL;
    if(kata[len_kata-1] == '=') len_hasil--;
    if(kata[len_kata-2] == '=') len_hasil--;

    char *hasil = malloc(len_hasil+1);
    int i, j;

    for(i=0, j=0; i<len_kata; i=i+4, j=j+3) {
        b64_part[0] = b64_cek(kata[i]);
        b64_part[1] = b64_cek(kata[i+1]);
        b64_part[2] = b64_cek(kata[i+2]);
        b64_part[3] = b64_cek(kata[i+3]);

        if(kata[i+1] != '=') hasil[j] = (b64_part[0] << 2) | (b64_part[1] >> 4);
        if(kata[i+2] != '=') hasil[j+1] = (b64_part[1] << 4) | (b64_part[2] >> 2);
        if(kata[i+3] != '=') hasil[j+2] = (b64_part[2] << 6) | b64_part[3];

    }
    
    hasil[len_hasil] = '\0';
    return hasil;

}
```
fungsi b64_decode digunakan untuk men-decode string kata yang dimasukkan dalam fungsi. Fungsi ini memanfaatkan fungsi b64_cek sebelumnya sebagai bagian dari dalam prosesnya.

```c
struct daftarUser {
    char nama[20];
    char pass[20];
};

struct daftarUser pengguna[20];
struct daftarUser userNow;
int jmlahUser = 0;
```
Deklarasi struct daftarUser ini digunakan untuk membentuk struktur user yang terdiri dari username dan password. Struct daftarUser digunakan sebanyak dua kali, yang pertama digunakan dalam bentuk array untuk menyatakan nama-nama pengguna yang terdaftar dalam users.txt, sementara yang kedua digunakan dalam variabel userNow untuk menyimpan data user yang telah melakukan login.

```c
void userlist() {
    jmlahUser = 0;
    char buffah[100];
    FILE *opened = fopen(userfile, "r");
    
    while(fgets(buffah, sizeof(buffah), opened) != NULL) {
        char *cheng;
        if((cheng = strchr(buffah, '\n')) != NULL) *cheng = '\0';

        char *nama = strtok(buffah, ":");
        char *pass = strtok(NULL, ":");

        char *newpass = b64_decode(pass);

        strcpy(pengguna[jmlahUser].nama, nama);
        strcpy(pengguna[jmlahUser].pass, newpass);
        free(newpass);
        jmlahUser++;
    }

    fclose(opened);
}
```
Fungsi userlist digunakan untuk mengambil semua nama user dan passwordnya dari file users.txt. Dalam 1kali putaran loop, fungsi ini akan mengambil sebuah baris dari file users.txt dan memisahkan line tersebut berdasarkan karakter `:`. Bagian pertama berisi username dan akan dimasukkan dalam pengguna[i].nama sementara yang kedua berisi password yang terenkripsi dan akan diproses menggunakan fungsi b64_decode sebelum kemudian dimasukkan dalam pengguna[i].pass.

```c
void creds() {
    userlist();
    for(int i = 0; i<jmlahUser; i++) {
        printf("Username: %s, Password: %s\n", pengguna[i].nama, pengguna[i].pass);
    }
    return;
}
```
fungsi creds adalah fungsi yang dipanggil ketika sender mengirimkan message `CREDS`. Fungsi ini akan memanggil fungsi userlist dan mengeprint semua nama user dan password yang terdaftar dari array struct pengguna.

```c
int loging(const char *uname, const char *passw) {
    int i = 0;
    for(i = 0; i<jmlahUser; i++) {
        int adaNama = strcmp(uname, pengguna[i].nama);

        if (adaNama == 0) {
            if(strcmp(passw, pengguna[i].pass) == 0) {
                strcpy(userNow.nama, uname);
                strcpy(userNow.pass, passw);
                
                if(userNow.nama[0] != '\0') printf("Authentication Successful\n");
                return 1;
            } else {
                printf("Authentication Failed\n");
                return 0;
            }
        }
    }
    if(i == jmlahUser && userNow.nama[0] == '\0') printf("Authentication Failed\n");
    return 0;
}
```
Fungsi loging adalah fungsi yang dipanggil ketika user memasukkan perintah `AUTH: username password`. Fungsi ini akan menerima 2 input berupa username dan password dan melakukan loop untuk mencari apakah nama user yang dimasukkan ada di dalam struct pengguna dan apakah password yang dimasukkan sesuai dengan password user tersebut.

```c
int fileAvailable(const char *fileToSen) {
    char senDir[50] = "Sender/";
    char recivDir[40] = "Receiver/";
    strcat(senDir, fileToSen);
    strcat(recivDir, fileToSen);
    FILE* fileptr;

    fileptr = fopen(senDir, "r");
    if(fileptr == NULL) {
        printf("%s is Not Exist\n", fileToSen);
        fclose(fileptr);
        return 0;
    }
    
    fileptr = fopen(recivDir, "r");
    if(fileptr != NULL) {
        printf("%s is Already Sended\n", fileToSen);
        fclose(fileptr);
        return 0;
    }

    return 1;
}
```
fungsi filecek adalah fungsi pertama dalam proses ketika pengguna memasukkan perintah `TRANSFER filename`. Fungsi ini akan memeriksa file di dalam directory Sender dan Receiver. Apabila file yang akan ditransfer sudah ada di directory Sender, maka fungsi akan mengirimkan pesan gagal mengirim. Sementara apabila file yang akan ditransfer tidak tersedia di directory Receiver, maka fungsi juga akan mengirimkan pesan gagal mengirim. Fungsi hanya akan mengirimkan kode 1 sebagai kode berhasil, yaitu ketika file terdapat di directory Sender dan tidak terdapat di directory Receiver.

```c
off_t cekSize(const char *namaFile) {
    struct stat fileStat;
    if(stat(namaFile, &fileStat) == 0) {
        return fileStat.st_size;
    }
    return 0;
}
```
Fungsi cekSize digunakan untuk mengukur ukuran file dalam satuan KB.

```c
void sendFile(const char *oneFile) {
    if(userNow.nama[0] == '\0') {
        printf("Please login first\n");
        return;
    }

    char senDir[50] = "Sender/";
    char recivDir[50] = "Receiver/";
    strcat(senDir, oneFile);
    strcat(recivDir, oneFile);
    
    if(fileAvailable(oneFile)) {
        off_t filSiz = cekSize(senDir);

        pid_t cabang = fork();
        if(cabang == 0) {
            char *argv[] = {"cp", senDir, recivDir, NULL};
            execv("/bin/cp", argv);
        } else {
            printf("%s (%jd KB) Has Been Sended\n", oneFile, filSiz);
        }
    }

}
```
Fungsi sendFile digunakan untuk mengirimkan file dari directory Sender ke receiver. Fungsi menerima input berupa nama file yang akan dikirim, Memeriksa ketersedian file dari fungsi filecek, mengambil ukuran file dari fungsi cekSize, dan kemudian membentuk fork untuk menjalankan perintah linux `cp` untuk mengirimkan filenya.

```c
struct received {
    long msg_tipe;
    char msg_msg[100];
} kiriman;
```
Struct received ini merupakan struct yang berisi data pesan dan tipenya. Data pesan diambil dari message queue yang dikirimkan oleh sender sebelumnya.

```c
int main() {
    key_t uniqKey;
    int msg_id;

    uniqKey = ftok("soal_3", 69);
    msg_id = msgget(uniqKey, 0666 | IPC_CREAT);

    while(1) {
        char koman[100];
        msgrcv(msg_id, &kiriman, sizeof(kiriman), 1, 0);
        strcpy(koman, kiriman.msg_msg);

        strtok(koman, "\n");
        char *arg1 = strtok(koman, " ");
        
        if(strcmp(arg1, "CREDS") == 0) {
            creds();

        } else if(strcmp(arg1, "AUTH:") == 0) {
            arg1 = strtok(NULL, " ");
            char *arg2 = arg1;
            arg1 = strtok(NULL, " ");
            char *arg3 = arg1;

            if(arg2 == NULL || arg3 == NULL) {
                printf("UNKNOWN COMMAND\n");
                return 0;
            }
            int sttus = loging(arg2, arg3);
            if(sttus == 0) break;
            
        } else if(strcmp(arg1, "TRANSFER") == 0) {
            arg1 = strtok(NULL, " ");
            char *arg2 = arg1;

            sendFile(arg2);
        } else if(strcmp(arg1, "QUIT") == 0) {
            break;
        } else {
            printf("UNKNOWN COMMAND\n");
        }
    }

    printf("Turned off the Receiver...\n");
    msgctl(msg_id, IPC_RMID, NULL);
    return 0;
}
```
Fungsi main merupakan fungsi utama yang berguna untuk mengambil pesan dari message queue dan mengarahkannya ke arah perintah yang akan dijalankan. Perintah yang terdaftar sama seperti yang tertera pada soal dengan tambahan perintah `QUIT` untuk menutup message queue dan mematikan file sender.
