#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#define ROWS1 2
#define COLS1 2
#define ROWS2 2
#define COLS2 2

unsigned long long factorial(int n) {
    unsigned long long hasil = 1;
    for (int i = 1; i <= n; i++) {
        hasil *= i;
    }
    return hasil;
}

void *calculate_factorial(void *arg) {
    int *data = (int *)arg;
    data[0] = factorial(data[0]);
    pthread_exit(NULL);
}

int main() {
    int matrix1[ROWS1][COLS1];
    int matrix2[ROWS2][COLS2];

    srand(time(NULL));
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS1; j++) {
            matrix1[i][j] = rand() % 4 + 1;
        }
    }

    for (int i = 0; i < ROWS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            matrix2[i][j] = rand() % 5 + 1;
        }
    }

    key_t key;

    key = ftok("belajar.c", 'R');
    int shmid = shmget(key, sizeof(int) * ROWS1 * COLS2, 0666 | IPC_CREAT);
    int *result = (int *)shmat(shmid, (void *)0, 0);

    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            result[i * COLS2 + j] = 0;
            for (int k = 0; k < COLS1; k++) {
                result[i * COLS2 + j] += matrix1[i][k] * matrix2[k][j];
            }
            result[i * COLS2 + j] -= 1;
        }
    }

    printf("Matriks Hasil:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", result[i * COLS2 + j]);
        }
        printf("\n");
    }

    int transpose[COLS2][ROWS1];
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            transpose[j][i] = result[i * COLS2 + j];
        }
    }

    printf("Matriks Hasil Transpose:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < ROWS1; j++) {
            printf("%d ", transpose[i][j]);
        }
        printf("\n");
    }

    for(int i=0; i<COLS2;i++) {
        for(int j=0; j<ROWS2;j++) {
            result[i * COLS2 + j] = transpose[i][j];
        }
    }

    shmdt(result);

    return 0;
}