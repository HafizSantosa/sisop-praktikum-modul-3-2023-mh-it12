#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#define ROWS1 2
#define COLS2 2

unsigned long long factorial(int n) {
    unsigned long long hasil = 1;
    for (int i = 1; i <= n; i++) {
        hasil *= i;
    }
    return hasil;
}

void *calculate_factorial(void *arg) {
    int *data = (int *)arg;
    data[0] = factorial(data[0]);
    pthread_exit(NULL);
}

int main() {
    clock_t tic = clock();
    key_t key = ftok("belajar.c", 'R');

    int shmid = shmget(key, sizeof(int) * ROWS1 * COLS2, 0666);
    int *result = (int *)shmat(shmid, (void *)0, 0);
    
    pthread_t threads[ROWS1 * COLS2];
    int thread_args[ROWS1 * COLS2];

    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            thread_args[i * COLS2 + j] = result[i * COLS2 + j];
            pthread_create(&threads[i * COLS2 + j], NULL, calculate_factorial, &thread_args[i * COLS2 + j]);
        }
    }
    for (int i = 0; i < ROWS1 * COLS2; i++) {
        pthread_join(threads[i], NULL);
    }

    printf("Matriks Hasil Faktorial:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%llu ", (unsigned long long)thread_args[i * COLS2 + j]);
        }
        printf("\n");
    }

    clock_t toc = clock();
    printf("\nprogram berjalan selama %f detik.\n", (double)(toc-tic) / CLOCKS_PER_SEC );
    
    shmdt(result);

    return 0;
}