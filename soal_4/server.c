#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

#define MAX_CLIENTS 5
#define PORT 8585
#define BUFFER_SIZE 1024

int main() {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    char buffer[BUFFER_SIZE] = {0};
    int clients[MAX_CLIENTS];

    for (int i = 0; i < MAX_CLIENTS; i++) {
        clients[i] = 0;
    }

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) == -1) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, MAX_CLIENTS) == -1) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    printf("Server is listening on port %d\n", PORT);

    while (1) {
        printf("\nWaiting for a new connection...\n");

        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) == -1) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        printf("Connection accepted from %s:%d\n", inet_ntoa(address.sin_addr), ntohs(address.sin_port));

        int i;
        for (i = 0; i < MAX_CLIENTS; i++) {
            if (clients[i] == 0) {
                clients[i] = new_socket;
                break;
            }
        }

        while (1) {
            memset(buffer, 0, BUFFER_SIZE);
            int read_size = read(new_socket, buffer, BUFFER_SIZE);
            if (read_size == -1) {
                perror("read");
                exit(EXIT_FAILURE);
            } else if (read_size == 0) {
                printf("Client disconnected\n");
                break;
            }

            printf("Message from client %s:%d: %s\n", inet_ntoa(address.sin_addr), ntohs(address.sin_port), buffer);

            for (int j = 0; j < MAX_CLIENTS; j++) {
                if (clients[j] != 0 && clients[j] != new_socket) {
                    if (write(clients[j], buffer, strlen(buffer)) == -1) {
                        perror("write");
                    }
                }
            }
        }
    }

    return 0;
}
