#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>

#define LOG_FILE "frekuensi.log"

void addLog(const char* tipe, const char* teks) {
    time_t terkini;
    char timestamp[20];

    time(&terkini);
    strftime(timestamp, 20, "[%d/%m/%y %H:%M:%S]", localtime(&terkini));

    FILE* log_file = fopen(LOG_FILE, "a");
    if (log_file) fprintf(log_file, "%s [%s] %s\n", timestamp, tipe, teks);
    fclose(log_file);
}

void getLirik() {
    FILE* cekFile = fopen("lyrics.txt", "r");
    char cmdDonlod[256];

    if(!cekFile) {
        fclose(cekFile);
        snprintf(cmdDonlod, sizeof(cmdDonlod), "wget -O lyrics.txt 'https://drive.google.com/uc?id=16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW'");
        system(cmdDonlod);
    }
}

void getOutput() {
    getLirik();

    FILE *input_file = fopen("lyrics.txt", "r");
    FILE *output_file = fopen("thebeatles.txt", "w");
    int k;

    if (input_file == NULL || output_file == NULL) {
        perror("Failed to open file");
        exit(1);
    }

    while ((k = fgetc(input_file)) != EOF) {
        if (isalpha(k) || k == ' ' || k == '\n') {
            if (isupper(k)) k = tolower(k);
            fputc(k, output_file);
        }
    }
    fclose(input_file);
    fclose(output_file);
}

void hitungHruf(const char* hruuf) {
    char fileCek[100];
    char riilHruuf = hruuf[0];
    int jmlah = 0;
    sprintf(fileCek, "thebeatles.txt");
    FILE *toCek = fopen(fileCek, "r");

    if (toCek == NULL) {
        perror("open file error\n");
        exit(1);
    }

    int c;
    while ((c = fgetc(toCek)) != EOF) {
        if (c == riilHruuf) jmlah++;
    }
    fclose(toCek);

    char mesg[512];
    snprintf(mesg, sizeof(mesg), "Huruf '%c' muncul sebanyak %d kali dalam file '%s'", riilHruuf, jmlah, fileCek);

    addLog("HURUF", mesg);
    printf("%s\n", mesg);
}

void hitungKta(const char* word) {
    char fileCek[100];
    char cariKata[256];
    char kataNow[256] = {0};
    int jmlah = 0;
    int inKata = 0;
    int pnjangKata = 0;

    sprintf(fileCek, "thebeatles.txt");
    sprintf(cariKata, " %s ", word);

    FILE *toCek = fopen(fileCek, "r");
    if (toCek == NULL) {
        perror("open file error\n");
        exit(1);
    }

    int c;
    while ((c = fgetc(toCek)) != EOF) {
        if (isalpha(c)) {
            kataNow[pnjangKata++] = c;
            inKata = 1;
        } else {
            if (inKata) {
                kataNow[pnjangKata] = '\0';
                if (strcmp(kataNow, word) == 0) jmlah++;

                pnjangKata = 0;
                inKata = 0;
            }
        }
    }
    fclose(toCek);

    char mesg[512];
    snprintf(mesg, sizeof(mesg), "Kata '%s' muncul sebanyak %d kali dalam file '%s'", word, jmlah, fileCek);
    addLog("KATA", mesg);
    printf("%s\n", mesg);
}

int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) {
        printf("Format: %s [<-kata> atau <-huruf>]\n", argv[0]);
        return 1;
    }

    getOutput();

    int pipefd[2];
    pipe(pipefd);
    pid_t child_pid = fork();

    if (child_pid == 0) {
        close(pipefd[0]);

        if (strcmp(argv[1], "-huruf") == 0) {
            char letter[256];
            printf("Masukkan huruf: ");
            scanf("%s", letter);
            write(pipefd[1], letter, strlen(letter) + 1);

        } else if (strcmp(argv[1], "-kata") == 0) {
            char word[256];
            printf("Masukkan kata: ");
            scanf("%s", word);
            write(pipefd[1], word, strlen(word) + 1);

        }
        close(pipefd[1]);
        exit(0);

    } else {
        close(pipefd[1]);
        int status;
        wait(&status);

        char dicari[256];
        read(pipefd[0], dicari, 256);

        if (strcmp(argv[1], "-huruf") == 0) hitungHruf(dicari);
        else if (strcmp(argv[1], "-kata") == 0) hitungKta(dicari);
    }

    return 0;
}